<div class="row">
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-teal panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-home color-blue"></em>
                <div class="large">8</div>
                <br>
                <div class="text-muted">Total Classes</div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-blue panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-clock-o color-orange"></em>
                <div class="large">12 H</div>
                <br>
                <div class="text-muted">Total Class Time</div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-orange panel-widget border-right">
            <div class="row no-padding"><em class="fa fa-xl fa-book color-teal"></em>
                <div class="large"></div>
                <br>
                <div class="text-muted"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3 col-lg-3 no-padding">
        <div class="panel panel-red panel-widget ">
            <div class="row no-padding"><em class="fa fa-xl fa-users color-red"></em>
                <div class="large">54</div>
                <br>
                <div class="text-muted">Total Students</div>
            </div>
        </div>
    </div>
</div><!--/.row-->