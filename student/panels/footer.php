<div class="row">
    <div class="col-sm-12">
        <p class="back-link">Designed by <a href="https://www.github.com/BlackIQ">Amirhossein Mohammadi</a> <i class="fa fa-heart color-red"></i></p>
        <p class="back-link">Everything Belongs To <a href="https://www.github.com/BlackIQ">Atieh</a> <i class="fa fa-copyright"></i> 2021</p>
        <p class="back-link">
            <i class="fa fa-telegram text-primary"></i>
            <i class="fa fa-instagram text-danger"></i>
            <i class="fa fa-twitter text-primary"></i>
            <i class="fa fa-facebook text-info"></i>
        </p>
    </div>
</div><!--/.row-->